package io.thalesdigital.candidate;

public final class ContactDetails {
    private String address;
    private String email;
    private String phoneNumber;

    public ContactDetails(String address, String email, String phoneNumber) {
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
}
