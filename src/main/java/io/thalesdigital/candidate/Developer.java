package io.thalesdigital.candidate;

import io.thalesdigital.company.TDFLocations;

import java.util.List;

// TODO: split interface into smaller interface
public interface Developer {
  enum SkillsLevel {
    NONE,
    WANT_TO_LEARN,
    AUTONOMOUS,
    TEACHER
  }

  enum LanguageLevel {
    A1,
    A2,
    B1,
    B2,
    C1,
    C2
  }

  int yearOfExperiencesIn(String development);

  boolean isTeamPlayer();

  boolean canLeadByExample();

  boolean seeksExcellence();

  boolean trainsTeamMembers();

  SkillsLevel getFullStackLevel();

  SkillsLevel practicesCleanCode();

  List<String> getLearningStrategies();

  SkillsLevel getTDDPracticeLevel();

  SkillsLevel getAgileLevel();

  SkillsLevel getBDDLevel();

  LanguageLevel getLanguageLevel(String language);

  boolean isProgrammerExpert();

  boolean isSoftwareCraftsman();

  boolean canReduceTechnicalDebt();

  boolean understands(String... concepts);

  boolean canMoveTo(TDFLocations location);

  ContactDetails getContactDetails();

  int countBonusSkills(String... skills);
}
