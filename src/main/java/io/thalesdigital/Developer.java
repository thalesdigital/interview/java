package io.thalesdigital;

// TODO: split interface into smaller interface
interface Developer {
  boolean isTeamPlayer();

  SkillsLevel getFullStackLevel();

  boolean practiceCleanCode();

  boolean canManageComplexity();

  boolean canLeadByExample();

  String getLearningStrategy();

  int yearOfExperiencesIn(String development);

  boolean seekExcellence();

  boolean trainTeamMembers();

  boolean understands(String... concepts);

  enum SkillsLevel {
    NONE,
    WANT_TO_LEARN,
    AUTONOMOUS,
    TEACHER
  }
  boolean isProgrammerExpert();

  boolean isSoftwareCrafter();

  boolean canReduceTechnicalDebt();

  boolean getTDDPracticeLevel();

  boolean canImproveTeamSkills();

  boolean knowsAgile();

  boolean knowsBDD();

  boolean canMoveTo(String city, String country);

  int count_bonus_skills(String... skills);
}
