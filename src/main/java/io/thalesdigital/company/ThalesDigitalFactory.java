package io.thalesdigital.company;

import io.thalesdigital.candidate.Developer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ThalesDigitalFactory {
    private final List<Developer> employees;
    private final List<String> employeesAutonomyFields;
    private final List<String> typeOfMissions;
    private final List<String> thalesFieldsOfActivity;

    public ThalesDigitalFactory() {
        employees = new ArrayList<Developer>();
        employeesAutonomyFields = new ArrayList<String>();
        typeOfMissions = new ArrayList<String>();
        thalesFieldsOfActivity = new ArrayList<String>();

        Collections.addAll(employeesAutonomyFields, "product design", "product development", "product maintenance", "product operation");
        Collections.addAll(typeOfMissions, "Improve the platform", "Work on MVPs", "Help Digital Transformation of Thales Group");
        Collections.addAll(thalesFieldsOfActivity, "Satellites", "Aeronautic", "Radars", "... and a lot of others");
    }

    public void hires(Developer developer) {
        employees.add(developer);
    }

    public boolean givesAutonomy(Developer developer, String field) {
        if (employees.contains(developer)) {
            return employeesAutonomyFields.contains(field);
        }
        return false;
    }

    public boolean improvesThanksTo(Developer developer) {
        return this.employees.contains(developer);
    }

    public List<String> getMissionsOf(Developer developer) {
        if (employees.contains(developer)) {
            return typeOfMissions;
        }
        return Collections.EMPTY_LIST;
    }

    public List<String> getFieldOfActivity() {
        return thalesFieldsOfActivity;
    }
}
