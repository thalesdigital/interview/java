package io.thalesdigital.company;

public enum TDFLocations {
    PARIS_FRANCE("Paris", "France"),
    MONTREAL_CANADA("Montreal", "Canada"),
    SINGAPORE("Singapore", "Singapore");

    String city;
    String country;

    TDFLocations(String city, String country) {
        this.city = city;
        this.country = country;
    }
}
