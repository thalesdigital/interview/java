package io.thalesdigital;

import io.thalesdigital.candidate.Developer;
import io.thalesdigital.company.ThalesDigitalFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ThalesDigitalFactoryTest {
    private ThalesDigitalFactory tdf = new ThalesDigitalFactory();
    private Developer candidate;

    private void given_candidate_has_been_recruited(Developer candidate) {
        tdf.hires(candidate);
        this.candidate = candidate;
    }

    @BeforeAll
    void setup() {
        Developer you = null; // TODO: instanciate developer
        given_candidate_has_been_recruited(you);
    }

    @Test
    void they_will_be_autonomous() {
        assertTrue(tdf.givesAutonomy(candidate, "product design"));
        assertTrue(tdf.givesAutonomy(candidate, "product development"));
        assertTrue(tdf.givesAutonomy(candidate, "product maintenance"));
        assertTrue(tdf.givesAutonomy(candidate, "product operation"));
    }

    @Test
    void they_will_have_various_missions() {
        assertThat(tdf.getMissionsOf(candidate)).contains("Improve the platform", "Work on MVPs", "Help Digital Transformation of Thales Group");
        assertThat(tdf.getFieldOfActivity()).contains("Satellites", "Aeronautic", "Radars", "... and a lot of others");
    }

    @Test
    void they_will_contribute_to_continuous_improvement() {
        assertTrue(tdf.improvesThanksTo(candidate));
    }

}
