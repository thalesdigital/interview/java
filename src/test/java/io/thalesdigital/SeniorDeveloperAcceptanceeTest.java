/**
 * QUI SOMMES NOUS ?
 *
 * <p>Située dans un espace de co-working, au cœur de Paris et plongée dans le monde des startups, Thales Digital
 * Factory apporte un support opérationnel à la mise en œuvre de la transformation digitale de Thales.
 *
 * <p>Pour accélérer cette transformation, Thales Digital Factory a été créée. Elle a pour objectif d’apporter un
 * support opérationnel à la mise en œuvre de la transformation digitale de Thales et s'articule autour de quatre pôles
 * : • Une plateforme Digitale. • Un centre d'excellence réunissant des expertises de classe mondiale sur les quatre
 * technologies clés du digital : la connectivité, le Big Data, l'intelligence artificielle et la cybersécurité. • Une «
 * DigitalAcademy» qui va faire rayonner la culture digitale et un incubateur qui hébergera des start-ups internes et
 * externes.
 *
 * <p>QUI ETES VOUS ?
 *
 * <p>• Vous avez une expérience de 6 ans minimum en tant que Ingénieur.e Développement Logiciel dans une organisation
 * en mode Agile. • Vous avez pour objectif de mettre en Prod vos développements le plus rapidement possible. • Vous
 * avez une grande appétence pour le DevOps, CI/CD, … • Vous écrivez du beau code : lisible, facile à maintenir,
 * compréhensible… • Vous mettez un point d’honneur à contribuer à l’autonomie des autres équipes en participant
 * activement aux projets inner source et open source de la Factory (ex : design system, scaffold…) . Ainsi les autres
 * équipes peuvent réutiliser nos assets en mode self-services. • Vous avez mené des projets pro et perso dont vous êtes
 * fier.e. • Vous avez envie de mettre en pratique/tester de nouveaux langages ou bonnes pratiques de dev chaque jour. •
 * Vous aimez travailler en équipe au quotidien. Pour vous le succès n’est que collectif. • Vous avez un niveau C1 en
 * anglais et rejoindre un environnement multiculturel vous motive.
 *
 * <p>Vous vous reconnaissez? Alors parlons missions ...
 *
 * <p>CE QUE NOUS POUVONS ACCOMPLIR ENSEMBLE
 *
 * <p>En nous rejoignant, vous vous verrez confier les missions suivantes : • Considéré (quasiment) comme les membres
 * d’une micro entreprise au sein de votre squad ; vous aurez l’autonomie pour décider de la façon dont vous designez,
 * développez, opérez et maintenez les produit digitaux sur notre plateforme Cloud (Azure) • Au travers des produits
 * auxquels vous contribuerez au sein de votre équipe, vous aurez un impact direct sur les services du Groupe Thales et
 * par conséquent sur des millions d’utilisateurs. • En parallèle de vos projets d’équipe, vous pourrez aussi contribuer
 * à l’amélioration continue de notre organisation, Thales Digital Factory. • By the way, c’est petit dej tous les
 * vendredi, BBQ l’été quand vous le souhaitez … Nous sommes toujours en phase? Alors candidatez et donnez-vous la
 * chance de rejoindre notre équipe.
 */
package io.thalesdigital;

import io.thalesdigital.candidate.Developer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.thalesdigital.candidate.Developer.LanguageLevel.B2;
import static io.thalesdigital.candidate.Developer.SkillsLevel.*;
import static io.thalesdigital.company.TDFLocations.*;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SeniorDeveloperAcceptanceTest {
  private Developer candidate;

  @BeforeAll
  void setup() {
    candidate = null; // TODO: make implementation here;
  }

  @Test
  void candidate_should_be_senior_developer() {
    assertThat(candidate.yearOfExperiencesIn("Software Developer")).isGreaterThan(6);
    assertTrue(candidate.isSoftwareCraftsman());
    assertTrue(candidate.isProgrammerExpert());
  }

  @Test
  void candidate_should_have_technical_skills() {
    assertThat(candidate.getFullStackLevel()).isGreaterThan(AUTONOMOUS);
    assertThat(candidate.practicesCleanCode()).isGreaterThan(WANT_TO_LEARN);
    assertThat(candidate.getTDDPracticeLevel()).isGreaterThan(WANT_TO_LEARN);
    assertTrue(candidate.understands("hexagonal architecture", "DDD", "unit tests"));
    assertTrue(candidate.canReduceTechnicalDebt());
  }

  @Test
  void candidate_should_learn() {
    assertThat(candidate.getLearningStrategies()).containsAnyOf("read", "meetup", "conference", "video", "katas");
  }

  @Test
  void candidate_should_have_teaching_skills() {
    assertTrue(candidate.trainsTeamMembers());
    assertTrue(candidate.canLeadByExample());
  }

  @Test
  void candidate_should_have_human_behavior() {
    assertTrue(candidate.isTeamPlayer());
    assertTrue(candidate.seeksExcellence());
  }

  @Test
  void candidate_should_have_methodologies_skills() {
    assertThat(candidate.getAgileLevel()).isGreaterThan(WANT_TO_LEARN);
    assertThat(candidate.getBDDLevel()).isGreaterThan(WANT_TO_LEARN);
  }

  @Test
  void candidate_should_be_geographically_mobile() {
    assertTrue(candidate.canMoveTo(PARIS_FRANCE));
  }

  @Test
  void candidate_should_works_in_multicultural_environment() {
    assertThat(candidate.getLanguageLevel("english")).isGreaterThan(B2);
  }

  @Test
  void candidate_should_have_some_bonus_skills() {
    assertThat(candidate.countBonusSkills("devops", "ci/cd", "lean startup", "haskel")).isGreaterThan(0);
  }

}

